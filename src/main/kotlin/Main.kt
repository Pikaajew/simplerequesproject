
import controller.DateController
import io.javalin.Javalin
import service.DateService

fun main() {
    val app = Javalin.create().start(80)
    val service = DateService()
    val controller = DateController(service)
    app.get("/", controller.get)
}