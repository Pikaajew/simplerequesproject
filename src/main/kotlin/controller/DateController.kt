package controller

import com.fasterxml.jackson.databind.ObjectMapper
import com.fasterxml.jackson.module.kotlin.KotlinModule
import io.javalin.core.util.Util.writeResponse
import io.javalin.http.Context
import io.javalin.http.Handler
import service.DateService

class DateController(private val dateService: DateService) {
    private val mapper = ObjectMapper().registerModule(KotlinModule())

    val get = Handler { ctx: Context ->
        val yearParam = ctx.queryParam("year")
        val currentDateParam = ctx.queryParam("currentDate")
        if (yearParam != null && currentDateParam != null) {
            ctx.res.sendError(400)
        } else if (yearParam != null) {
            val year = yearParam.toIntOrNull()
            val response = dateService.calculateDay256(year)
            writeResponse(ctx.res, mapper.writeValueAsString(response), 200)
        } else if (currentDateParam != null) {
            val response = dateService.findNextDayOfProgrammer(currentDateParam)
            writeResponse(ctx.res, mapper.writeValueAsString(response), 200)
        }
        ctx.res.sendError(400)
    }
}