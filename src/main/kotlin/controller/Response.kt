package controller

data class Response(val errorCode: Int, val dataMessage: String)