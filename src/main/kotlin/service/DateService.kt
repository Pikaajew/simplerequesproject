package service

import controller.Response
import java.lang.Exception
import java.sql.Date
import java.text.SimpleDateFormat
import java.util.*

class DateService {
    private val numbersOfDaysInMonthsOfYear = arrayOf(31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31)
    private val numbersOfDaysInMonthsOfLeapYear = arrayOf(31, 29, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31)

    fun calculateDay256(year: Int?): Response {
        if (year == null) {
            return Response(400, "")
        }
        if (year < 1) {
            return Response(400, "")
        }
        val date = dateFromDaysNumber(256, year)
        val dateFormat = SimpleDateFormat("dd/MM/yy")
        return Response(200, dateFormat.format(date))
    }

    fun findNextDayOfProgrammer(currentDate: String): Response {
        val day = try { currentDate.take(2).toInt() } catch (e: Exception) { null }
        val month = try { currentDate.substring(2, 4).toInt() } catch (e: Exception) { null }
        val year = try { currentDate.substring(4).toInt() } catch (e: Exception) { null }
        if (day == null || month == null || year == null) {
            return Response(400, "")
        }
        if (month > 12 || month < 1 || year < 1 || day < 1) {
            return Response(400, "")
        }
        val isLeapYear = isLeapYear(year)
        if (isLeapYear && day > numbersOfDaysInMonthsOfLeapYear[month - 1] ||
                !isLeapYear && day > numbersOfDaysInMonthsOfYear[month - 1]) {
            return Response(400, "")
        }
        val daysInYear = if (isLeapYear) 366 else 365
        var daysLeft = 0
        val months = if (isLeapYear) numbersOfDaysInMonthsOfLeapYear else numbersOfDaysInMonthsOfYear
        for (i in 11 downTo month) {
            daysLeft += months[i]
        }
        daysLeft += months[month - 1] - day // to year end
        daysLeft = if (daysLeft >= (daysInYear - 256)) daysLeft - (daysInYear - 256) else daysLeft + 256
        return Response(200, "$daysLeft")
    }

    private fun isLeapYear(year: Int): Boolean {
        if (year % 4 != 0) {
            return false
        }
        if (year % 100 == 0 && year % 400 != 0) {
            return false
        }
        return true
    }

    private fun dateFromDaysNumber(daysNumber: Int, year: Int): Date {
        val months = if (isLeapYear(year)) numbersOfDaysInMonthsOfLeapYear else numbersOfDaysInMonthsOfYear
        var month = 0
        var day = daysNumber
        for (daysNumberInMonth in months) {
            if (day < daysNumberInMonth) {
                break
            }
            month++
            day -= daysNumberInMonth
        }
        return Date(GregorianCalendar(year, month, day).time.time)
    }
}