import controller.Response
import org.junit.Test
import org.junit.Assert.assertEquals
import service.DateService
import java.sql.Date
import java.text.SimpleDateFormat

class DateServiceTest {

    @Test
    fun calculateDay256Test() {
        val dateService = DateService()
        val dateFormat = SimpleDateFormat("dd/MM/yy")

        val resultYear2017 = dateService.calculateDay256(2017)
        val expectedResultYear2017 = Response(200, dateFormat.format(Date.valueOf("2017-09-13")))
        assertEquals(expectedResultYear2017, resultYear2017)

        val resultYear2000 = dateService.calculateDay256(2000)
        val expectedResultYear2000 = Response(200, dateFormat.format(Date.valueOf("2000-09-12")))
        assertEquals(expectedResultYear2000, resultYear2000)

        val resultYear2020 = dateService.calculateDay256(2020)
        val expectedResultYear2020 = Response(200, dateFormat.format(Date.valueOf("2020-09-12")))
        assertEquals(expectedResultYear2020, resultYear2020)

        val resultYear2100 = dateService.calculateDay256(2100)
        val expectedResultYear2100 = Response(200, dateFormat.format(Date.valueOf("2100-09-13")))
        assertEquals(expectedResultYear2100, resultYear2100)
    }

    @Test
    fun findNextDayOfProgrammerTest() {
        val dateService = DateService()

        val result10092017 = dateService.findNextDayOfProgrammer("10092017")
        val expectedResult10092017 = Response(200, "3")
        assertEquals(expectedResult10092017, result10092017)

        val result30122019 = dateService.findNextDayOfProgrammer("30122019")
        val expectedResult30122019 = Response(200, "257")
        assertEquals(expectedResult30122019, result30122019)
    }
}